/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacion;

/**
 *
 * @author bayer
 */
public class Narrador {
    int edad;
    void narracion1(){
       System.out.println("Era una noche lluviosa, cuando la conoció, o al menos de eso se convenció a sí mismo en el primer momento en el que sus ojos se cruzaron bajo la lluvia ");
    }
    void narracion2(){
        System.out.println("El chico había esperado la oportunidad para iniciar una conversación apenas acabase la lluvia, pero ese hecho parecía cada vez más lejano, así que se llenó de valor y decidió comenzar una conversación ");
    }
    void narracion3(){
        System.out.println("Sin que el chico se diese cuenta la conversación se apagó como se apaga en algún momento la llama que nos da la vida, tan solo se podía distinguir el sonido de la lluvia, envolviendo el ambiente en un sórdido sonido y trayendo consigo una sensación melancólica para el muchacho que solo podía ver las gotas caer ");
    }
    void narracion4(){
        System.out.println("Pasado un tiempo el joven decidió seguir con la conversación, llenándose de valentía para luchar contra sí mismo pues su corazón le pedía a gritos que continuase pero su mente le rogaba “que saliese corriendo” sintiendo una presión agobiante que lo llenaba de incertidumbre… Sin embargo alzo la voz y dijo:   ");
    }
    void narracion5(){
        System.out.println("Rápidamente la lluvia ceso de manera abrupta, Anna se despidió con prisa, su cara bajo la luz de la luna era radiante, pálida y fría despertando en el muchacho el verla un sentimiento de nostalgia, solo dijo un “gracias”.  El muchacho no le pudo decir adiós, sintió tristeza al verla irse… antes de que saliese de su campo de visión el joven alzo la mano y se despidió, ella voltio para hacer lo mismo.   ");
    }
    void narracion6(){
        System.out.println("Se separaron.   Al siguiente día el muchacho se levantó llorando, recordando que fue Anna su mejor amiga en el instituto, que ella murió en un asalto hace dos años y que vino a decirle adiós como lo había prometido cuando eran pequeños. ¿Mi nombre? pregunta... jajajaja Eso no importa señora, yo tan solo reparto pizzas. ");
    }
}
